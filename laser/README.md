Nombre d'exemplaires pour chaque fichier:
* tab.svg: 12 exemplaires
* panneau-bas.svg: 1 exemplaire
* panneau-haut.svg: 1 exemplaire
* panneau-lateral.svg: 2 exemplaires

L'épaisseur des plaques de bois à utiliser pour toutes les choses à découper est 1/8po (3.175mm).

Dans ces trois fichiers, j'utilise les couleurs suivantes: 
* Le gris (#808080ff) pour mes formes dont le contour doit être découpé
* Le bourgogne (#aa0000ff) pour les morceaux qu'il faut découper de mes formes en gris
* Le bleu (#00ffffff) pour les parties que je veux que tu engrave très légèrement. Elles me servent uniquement de guide pour coller des pièces.

